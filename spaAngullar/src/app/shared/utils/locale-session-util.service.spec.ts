import { TestBed } from '@angular/core/testing';

import { LocaleSessionUtilService } from './locale-session-util.service';

describe('LocaleSessionUtilService', () => {
  let service: LocaleSessionUtilService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LocaleSessionUtilService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
