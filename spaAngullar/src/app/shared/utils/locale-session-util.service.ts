import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocaleSessionUtilService {

  constructor() { }


    
  
  getValueInSession(nameKey: string, value: any){
    return localStorage.getItem(nameKey);
  }

  setValueInSession(nameKey: string, value: any){
    return localStorage.setItem(nameKey, value);
  }

  getJsonInSession(nameKey: string){
    return JSON.parse(localStorage.getItem(nameKey));
  }

  setJsonInSession(nameKey: string, object: any){
    return localStorage.setItem(nameKey, JSON.stringify(object));
  }

  deleteObjectInSession(nameKey: string){
    return localStorage.removeItem(nameKey);
  }
}
