export interface IProduct {
    uid?: string;
    name?: string;
    description?: string;
    price?: string;
    url_image?: string;
    link_mp?: string; //link de mercado pago
    active?: boolean;
    type?: string; //corporal o facial
}
