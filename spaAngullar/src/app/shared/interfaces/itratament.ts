export interface ITratament {
    uid?: string;
    name?: string;
    description?: string;
    price?: string;
    url_image?: string;
    link_mp?: string; //link de mercado pago
    active?: boolean;
    type?: string; //nutricion o facial
}
