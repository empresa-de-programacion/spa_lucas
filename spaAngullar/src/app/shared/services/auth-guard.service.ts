import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { ESessionKeys } from '../enums/ESessionKeys';
import { LocaleSessionUtilService } from '../utils/locale-session-util.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private router: Router,
    private _localeSessionUtilService:LocaleSessionUtilService) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {

    console.log(route);

    let authInfo = {
      authenticated: false
    };
    
    if(this._localeSessionUtilService.getJsonInSession(ESessionKeys.USER_IN_SESSION)){
      authInfo.authenticated = true;
    }

    if (!authInfo.authenticated) {
      this.router.navigate(["/"]);
      return false;
    }

    return true;
  }
}
