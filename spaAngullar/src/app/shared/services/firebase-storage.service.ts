import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { TratamentService } from './tratament.service';

@Injectable({
  providedIn: 'root'
})
export class FirebaseStorageService {

  constructor(
    private _angularFireStorage: AngularFireStorage,
    private _tratamentService: TratamentService
  ) { }

  public saveImage(nameFile: string, data: any) {
    return this._angularFireStorage.upload(nameFile, data);
  }

  //subir un archivo
  //recibe una url que comienza y termina con '/'
  //recibe el nombre del archivo
  //recibe el archivo
  //despues devuelve la tarea de subida del archivo para tratar la la respuesta
  public saveFile(path: string,nameFile: string, data: any) {
    const ref = this._angularFireStorage.ref(`${path}${nameFile}`);
    return ref.put(data);
 }

   //Referencia del archivo
   public getReference(nameFile: string) {
    return this._angularFireStorage.ref(nameFile);
  }

  public deleteFile(pathName: string) {
    const ref = this._angularFireStorage.ref(`${pathName}`);
    return ref.delete();
  }

    
    //recibe el evento del input file
    //example of path : 
    //const path = `users/${this.userService.getSessionUser().uid}/`;
    //guarda la imagen y devuelve la url de la misma
    async changeImage(e,path: string,name: string): Promise<any>{
       
      const file = e.target.files[0];
      const pathName = `${path}${name}`;
      let imageUrl = '';
      return new Promise((resolve, reject) => {
        this.saveFile(path,name,file).then(
          () => {
              const reference =  this.getReference(pathName);
              reference.getDownloadURL().subscribe((url) => {
                imageUrl = url;
                resolve(imageUrl)
            });
          }).catch((error) => {
            reject(error);
          });
      });
    }


}
