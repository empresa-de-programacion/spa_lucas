import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
// import { ESessionKeys } from '../enums/ESessionKeys';
import firebase from 'firebase/app';
import { ESessionKeys } from '../enums/ESessionKeys';
import { IUser } from '../interfaces/IUser';
import { LocaleSessionUtilService } from '../utils/locale-session-util.service';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private angularFireAuth: AngularFireAuth,  private router: Router,
    private _localeSessionUtilService: LocaleSessionUtilService
  ) { }

   async logOut() {
    try {
      this._localeSessionUtilService.deleteObjectInSession(ESessionKeys.USER_IN_SESSION);
      await this.angularFireAuth.signOut();
      this.router.navigateByUrl("/");
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  loginWithMail(email, password) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(email, password)
      .then((user) => {
        resolve(user);
      })
      .catch((error) => {
        reject(error)
      });
    })
  }

  // async createUser(firebaseUserData: any) {
  //   let userToStore: IUser;
  //   userToStore = this.getGoogleUser(firebaseUserData);
  //   await this.userService.saveUser(userToStore).then(
  //     () => { console.log('Usuario creado correctamente'); }
  //   ).catch(
  //     (error) => { console.error(error); }
  //   );
  //   return userToStore;
  // }
  
 
  
}
