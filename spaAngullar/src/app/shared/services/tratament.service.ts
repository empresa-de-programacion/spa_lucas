import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ITratament } from '../interfaces/itratament';

@Injectable({
  providedIn: 'root'
})
export class TratamentService {

  constructor(
    private _angularFirestore: AngularFirestore,
  ) { }

  getTratamentsObservable() {
    return this._angularFirestore.collection('trataments').valueChanges();
  }

  getTrataments() {
    return new Promise<ITratament[]>((resolve, reject) => {
      return this._angularFirestore.collection('trataments').valueChanges().subscribe(
        (data) => {
         resolve(data as ITratament[]);
      }, (error) => {
         reject(error);
      });
    });
  }

  async createTratament(document){
    try {
      let tratament =  await this._angularFirestore.collection('trataments').add(document);
       document.uid = tratament.id;
       await this.editTratament(document);
       return document;
    } catch (error) {
      throw (error);
    }
  }

  async editTratament(document){
    const uid = document.uid;
    try {
      await this._angularFirestore.collection('trataments').doc(uid).set(document, { merge: true });
      return document;
   } catch (error) {
     throw (error);
   }
  }

  async deleteTratament(id){
    try {
      await this._angularFirestore.collection('trataments').doc(id).delete();
    } catch (error) {
      throw (error);
    }
  }

  toggleActiveTratament(document: ITratament){
    document.active = !document.active;
    this.editTratament(document);
  }


}
