import { TestBed } from '@angular/core/testing';

import { TratamentService } from './tratament.service';

describe('TratamentService', () => {
  let service: TratamentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TratamentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
