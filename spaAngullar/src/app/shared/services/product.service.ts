import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { IProduct } from '../interfaces/iproduct';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private _angularFirestore: AngularFirestore,
  ) { }

  getProductsObservable() {
    return this._angularFirestore.collection('products').valueChanges();
  }

  getProducts() {
    return new Promise<IProduct[]>((resolve, reject) => {
      return this._angularFirestore.collection('products').valueChanges().subscribe(
        (data) => {
         resolve(data as IProduct[]);
      }, (error) => {
         reject(error);
      });
    });
  }

  async createProduct(document){
    try {
      let profuct =  await this._angularFirestore.collection('products').add(document);
       document.uid = profuct.id;
       await this.editProduct(document);
       return document;
    } catch (error) {
      throw (error);
    }
  }

  async editProduct(document){
    const uid = document.uid;
    try {
      await this._angularFirestore.collection('products').doc(uid).set(document, { merge: true });
      return document;
   } catch (error) {
     throw (error);
   }
  }

  async deleteProduct(id){
    try {
      await this._angularFirestore.collection('products').doc(id).delete();
    } catch (error) {
      throw (error);
    }
  }

  toggleActiveProduct(document: IProduct){
    document.active = !document.active;
    this.editProduct(document);
  }


}
