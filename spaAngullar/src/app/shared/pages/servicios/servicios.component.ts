import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ITratament } from '../../interfaces/itratament';
import { TratamentService } from '../../services/tratament.service';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html',
  styleUrls: ['./servicios.component.css']
})
export class ServiciosComponent implements OnInit {


  nameQuery:string;
  typeQuery:string = "todos";
  constructor(private _tratamentService: TratamentService,
    private _activatedRoute: ActivatedRoute) { }
  allTrataments : ITratament[] = [];
  tratamentsFiltered : ITratament[] = [];
  loading = true;
  textLoading = "Cargando tratamientos...";

  async ngOnInit() {
    try {
      this.allTrataments = await this._tratamentService.getTrataments();
      this.tratamentsFiltered = this.allTrataments;
      this.loading = false;
      const type = this._activatedRoute.snapshot.queryParams['type'];
      if(type){
        this.typeQuery = type;
        this.filterTrataments();
      }
    } catch (error) {
      console.error(error);      
    }
  }

  filterTrataments(){
     
    let tratamentsAux = this.allTrataments;
    if(this.nameQuery === undefined || this.nameQuery === null  || this.nameQuery === ''){
      tratamentsAux = this.allTrataments;
    }else{
      tratamentsAux = tratamentsAux.filter(o => {
        if(o.name.toLocaleLowerCase().indexOf(this.nameQuery) > -1)
        {
          return o;
        }
      })
    }
    if(this.typeQuery != "todos"){
      tratamentsAux = tratamentsAux.filter(o => {
        if(o.type === this.typeQuery)
        {
          return o;
        }
      })
    }

    this.tratamentsFiltered = tratamentsAux;

    
  }



}
