import { Component, OnInit } from '@angular/core';
import { ITratament } from 'src/app/shared/interfaces/itratament';
import { FirebaseStorageService } from 'src/app/shared/services/firebase-storage.service';
import { TratamentService } from 'src/app/shared/services/tratament.service';

@Component({
  selector: 'app-abmtrataments',
  templateUrl: './abmtrataments.component.html',
  styleUrls: ['./abmtrataments.component.css']
})
export class ABMTratamentsComponent implements OnInit {

  loading = true;
  status = "list";
  hasDisableButton = false;
  file: File ;
  allTrataments: ITratament[] = [];
  tratamentModel: ITratament = {
    active: true,
    type: "corporal"
  };
  constructor(
    private _tratamentsService : TratamentService,
    private _firebaseStorageService : FirebaseStorageService,
    
  ) { }

  ngOnInit(): void {
    this._tratamentsService.getTratamentsObservable().subscribe(
      data => {
        this.allTrataments = data as ITratament[];
        this.loading = false;
      },
      error => {
        console.error(error);
      }
    );
  }


  handleClickEdit(tratament){
      this.tratamentModel ={
        ...tratament
      };
      this.status = "edit"
  }

  handleClickCreate(){
    this.status = "create";
}

  handleClickDelete(uid){
    try {
      this._tratamentsService.deleteTratament(uid);
    } catch (error) {
      console.error(error);      
    }
  }

  handleClickCancel(){
    this.tratamentModel =  {
      active: true,
      type: "corporal"
    };
    this.status = "list";
  }

  async handleClickSaveEdit(){
    try {
      this.loading = true;
     if(this.file){
       this. changeImage(this.file,this.tratamentModel,false);
      }
     await  this._tratamentsService.editTratament(this.tratamentModel);
     this.handleClickCancel();
    } catch (error) {
      console.error(error);      
    } finally {
      this.loading = false;
    }
  }

  async handleClickSaveCreate(){
    try {
      this.loading = true;
      if(this.file){
        this. changeImage(this.file,this.tratamentModel,false);
      }
      await this._tratamentsService.createTratament(this.tratamentModel);
      this.handleClickCancel();
    } catch (error) {
      console.error(error);      
    } finally {
      this.loading = false;
    }
  }

  //este metodo carga la imagen en el elemento
  loadImageProfile(){
    // const element = document.getElementById('image');
    // if(this.userForImage.urlPhoto){
    //   element.style.backgroundImage =`url(${this.userForImage.urlPhoto})`;
    // }else{
    //   const pathNoFile = '../../../assets/images/no_image.jpg';
    //   element.style.backgroundImage =`url(${pathNoFile})`;

    // }
  }

  handleMouseLeaveInputFile(e,tratament,isTemp) {
     
    if (e.target.files.length) {
      this.changeImage(e,tratament,isTemp);
    } else {
      console.log('No se subio ninguna imagen');
    }
  }

  async changeImage(e,tratament,isTemp){
    try{
      // this.loadingService.showLoading();
      this.loading = true;
      const id = tratament.uid ? tratament.uid : "new";
      const temp = isTemp ? 'temp/' : '';
      const path = `trataments/${temp}${id}/`;
      const name = 'tratamentImage';
      this.file = e;
       
      const url = await this._firebaseStorageService.changeImage(e,path,name);
      this.tratamentModel.url_image = url ;
    }catch(error){
      console.error(error);
    }finally{
      this.loading = false;
    }
  }


  handleClickChangeImg(){
    const inputFile = document.getElementById('image');
    inputFile.click();
  } 

  isValid(){
    if(this.tratamentModel.url_image && this.tratamentModel.type 
      && this.tratamentModel.price && this.tratamentModel.name 
      && this.tratamentModel.link_mp && this.tratamentModel.description){
        this.hasDisableButton = false;
      }else{
        this.hasDisableButton = true;
      }
  }   

  
  async handleClickDeleteImage(tratament: ITratament){
      if(tratament.url_image){
        try {
          // this.loadingService.showLoading();
           
          this.loading = true;
          const finalPath = tratament.url_image;
          await this._firebaseStorageService.deleteFile(finalPath);    
          this.tratamentModel.url_image = '';      
        } catch (error) {
          console.error(error);
        } finally{
          this.loading = false;
        }
      }
  }
}
  