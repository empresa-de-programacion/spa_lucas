import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ABMTratamentsComponent } from './abmtrataments.component';

describe('ABMTratamentsComponent', () => {
  let component: ABMTratamentsComponent;
  let fixture: ComponentFixture<ABMTratamentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ABMTratamentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ABMTratamentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
