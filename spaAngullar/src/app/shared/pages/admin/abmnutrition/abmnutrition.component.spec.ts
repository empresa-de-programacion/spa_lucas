import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ABMNutritionComponent } from './abmnutrition.component';

describe('ABMNutritionComponent', () => {
  let component: ABMNutritionComponent;
  let fixture: ComponentFixture<ABMNutritionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ABMNutritionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ABMNutritionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
