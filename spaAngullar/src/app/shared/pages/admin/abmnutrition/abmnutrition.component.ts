import { Component, OnInit } from '@angular/core';
import { IProduct } from 'src/app/shared/interfaces/iproduct';
import { ITratament } from 'src/app/shared/interfaces/itratament';
import { FirebaseStorageService } from 'src/app/shared/services/firebase-storage.service';
import { ProductService } from 'src/app/shared/services/product.service';
import { TratamentService } from 'src/app/shared/services/tratament.service';

@Component({
  selector: 'app-abmnutrition',
  templateUrl: './abmnutrition.component.html',
  styleUrls: ['./abmnutrition.component.css']
})
export class ABMNutritionComponent implements OnInit {

  loading = true;
  status = "list";
  hasDisableButton = false;
  file: File ;
  allProducts: IProduct[] = [];
  productModel: IProduct = {
    active: true,
    type: "nutrition" //puede ser nutrition o cosmetic
  };
  constructor(
    private _productService : ProductService,
    private _firebaseStorageService : FirebaseStorageService,
  ) { }

  ngOnInit(): void {
    this._productService.getProductsObservable().subscribe(
      data => {
        let products =  data as IProduct[];
        this.allProducts = products.filter( p => {
          if(p.type === "nutrition")
          {
            return p;
          }
        })
        this.loading = false;
      },
      error => {
        console.error(error);
      }
    );
  }


  handleClickEdit(product){
      this.productModel ={
        ...product
      };
      this.status = "edit"
  }

  handleClickCreate(){
    this.status = "create";
}

  handleClickDelete(uid){
    try {
      this._productService.deleteProduct(uid);
    } catch (error) {
      console.error(error);      
    }
  }

  handleClickCancel(){
    this.productModel =  {
      active: true,
      type: "nutrition"
    };
    this.status = "list";
  }

  async handleClickSaveEdit(){
    try {
      this.loading = true;
      this.hasDisableButton = true;
     if(this.file){
       this. changeImage(this.file,this.productModel,false);
      }
     await  this._productService.editProduct(this.productModel);
     this.handleClickCancel();
    } catch (error) {
      console.error(error);      
    }finally{
      this.loading = false;
    }
  }

  async handleClickSaveCreate(){
    try {
      this.loading = true;
      this.hasDisableButton = true;
      if(this.file){
        this.changeImage(this.file,this.productModel,false);
      }
      await this._productService.createProduct(this.productModel);
      this.handleClickCancel();
    } catch (error) {
      console.error(error);      
    }finally{
      this.loading = false;
    }
  }


  handleMouseLeaveInputFile(e,product,isTemp) {
     
    if (e.target.files.length) {
      this.changeImage(e,product,isTemp);
    } else {
      console.log('No se subio ninguna imagen');
    }
  }

  async changeImage(e,product,isTemp){
    try{
      this.loading = true;
      const id = product.uid ? product.uid : "new";
      const temp = isTemp ? 'temp/' : '';
      const path = `products/${temp}${id}/`;
      const name = 'productImage';
      this.file = e;
       
      const url = await this._firebaseStorageService.changeImage(e,path,name);
      this.productModel.url_image = url ;
    }catch(error){
      console.error(error);
    }finally{
      this.loading = false;
    }
  }


  handleClickChangeImg(){
    const inputFile = document.getElementById('image');
    inputFile.click();
  } 

  isValid(){
    if(this.productModel.url_image && this.productModel.type 
      && this.productModel.price && this.productModel.name 
      && this.productModel.link_mp && this.productModel.description){
        this.hasDisableButton = false;
      }else{
        this.hasDisableButton = true;
      }
  }   

  
  async handleClickDeleteImage(product: IProduct){
      if(product.url_image){
        try {
          // this.loadingService.showLoading();
           
          this.loading = true;
          const finalPath = product.url_image;
          await this._firebaseStorageService.deleteFile(finalPath);    
          this.productModel.url_image = '';      
        } catch (error) {
          console.error(error);
        } finally{
          this.loading = false;
        }
      }
  }
}
  