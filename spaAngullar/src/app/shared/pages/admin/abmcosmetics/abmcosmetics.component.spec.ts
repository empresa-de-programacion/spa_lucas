import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ABMCosmeticsComponent } from './abmcosmetics.component';

describe('ABMCosmeticsComponent', () => {
  let component: ABMCosmeticsComponent;
  let fixture: ComponentFixture<ABMCosmeticsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ABMCosmeticsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ABMCosmeticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
