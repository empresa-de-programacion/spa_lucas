import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ESessionKeys } from 'src/app/shared/enums/ESessionKeys';
import { LoginService } from 'src/app/shared/services/login.service';
import { LocaleSessionUtilService } from 'src/app/shared/utils/locale-session-util.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private _loginService : LoginService,
    private _localeSessionUtilService: LocaleSessionUtilService,
    private router: Router,

  ) { }

  email: string;
  password: string;
  disabled = true;
  loading = false;
  error: string;
  ngOnInit(): void {
  }

  async handleClickLogin(){
    try {
      this.loading = true;
      const user  = await this._loginService.loginWithMail(this.email,this.password);
      this._localeSessionUtilService.setJsonInSession(ESessionKeys.USER_IN_SESSION,user);
      this.router.navigateByUrl("/admin/tratamientos");
    } catch (error) {
      if(error.code === "auth/invalid-email"){
        this.error = "Credenciales inválidas."
      }else{
        console.error(error);
      }
    }finally{
      this.loading = false;
    }
  }

  isValid(){
    this.disabled = true;
    if(this.email && this.password){
      this.disabled = false;
    }

  }
}
