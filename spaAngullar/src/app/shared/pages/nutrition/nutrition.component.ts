import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ITratament } from '../../interfaces/itratament';
import { ProductService } from '../../services/product.service';
import { TratamentService } from '../../services/tratament.service';

@Component({
  selector: 'app-nutrition',
  templateUrl: './nutrition.component.html',
  styleUrls: ['./nutrition.component.css']
})
export class NutritionComponent implements OnInit {

  nameQuery:string;
  typeQuery:string = "todos";
  constructor(private _productService: ProductService ) { }
  allTrataments : ITratament[] = [];
  tratamentsFiltered : ITratament[] = [];
  loading = true;
  textLoading = "Cargando cosméticos...";

  async ngOnInit() {
    try {
      this.allTrataments = await this._productService.getProducts();
      this.tratamentsFiltered = this.allTrataments;
      this.loading = false;
      const type = "nutrition";
      if(type){
        this.typeQuery = type;
        this.filterTrataments();
      }
    } catch (error) {
      console.error(error);
    }
  }

  filterTrataments(){
     
    let tratamentsAux = this.allTrataments;
    if(this.nameQuery === undefined || this.nameQuery === null  || this.nameQuery === ''){
      tratamentsAux = this.allTrataments;
    }else{
      tratamentsAux = tratamentsAux.filter(o => {
        if(o.name.toLocaleLowerCase().indexOf(this.nameQuery) > -1)
        {
          return o;
        }
      })
    }
    if(this.typeQuery != "todos"){
      tratamentsAux = tratamentsAux.filter(o => {
        if(o.type === this.typeQuery)
        {
          return o;
        }
      })
    }

    this.tratamentsFiltered = tratamentsAux;

  }

}
