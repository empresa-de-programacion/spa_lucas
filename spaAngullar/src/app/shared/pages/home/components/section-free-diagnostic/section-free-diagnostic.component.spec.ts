import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionFreeDiagnosticComponent } from './section-free-diagnostic.component';

describe('SectionFreeDiagnosticComponent', () => {
  let component: SectionFreeDiagnosticComponent;
  let fixture: ComponentFixture<SectionFreeDiagnosticComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionFreeDiagnosticComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionFreeDiagnosticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
