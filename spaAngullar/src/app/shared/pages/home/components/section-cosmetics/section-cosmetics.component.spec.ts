import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionCosmeticsComponent } from './section-cosmetics.component';

describe('SectionCosmeticsComponent', () => {
  let component: SectionCosmeticsComponent;
  let fixture: ComponentFixture<SectionCosmeticsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionCosmeticsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionCosmeticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
