import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionBannerTextComponent } from './section-banner-text.component';

describe('SectionBannerTextComponent', () => {
  let component: SectionBannerTextComponent;
  let fixture: ComponentFixture<SectionBannerTextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionBannerTextComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionBannerTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
