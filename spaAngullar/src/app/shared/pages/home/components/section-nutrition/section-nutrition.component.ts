import { Component, OnInit } from '@angular/core';
import { IProduct } from 'src/app/shared/interfaces/iproduct';
import { ProductService } from 'src/app/shared/services/product.service';

@Component({
  selector: 'app-section-nutrition',
  templateUrl: './section-nutrition.component.html',
  styleUrls: ['./section-nutrition.component.css']
})
export class SectionNutritionComponent implements OnInit {


  loading = true;
  textLoading = "Cargando productos de nutrición... ";
  trataments : IProduct[];
  allTrataments : IProduct[];
  constructor(private _productService: ProductService) { }

  async ngOnInit() {
    this.allTrataments = await this._productService.getProducts();
    this.allTrataments = this.allTrataments.filter(data => data.type ==="nutrition")
    this.trataments = this.allTrataments.slice(0,3);
    this.loading = false;
  }

}
