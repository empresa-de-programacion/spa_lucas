import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionMediaSocialComponent } from './section-media-social.component';

describe('SectionMediaSocialComponent', () => {
  let component: SectionMediaSocialComponent;
  let fixture: ComponentFixture<SectionMediaSocialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionMediaSocialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionMediaSocialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
