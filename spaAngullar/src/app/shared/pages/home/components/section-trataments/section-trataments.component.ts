import { Component, OnInit } from '@angular/core';
import { ITratament } from 'src/app/shared/interfaces/itratament';
import { TratamentService } from 'src/app/shared/services/tratament.service';

@Component({
  selector: 'app-section-trataments',
  templateUrl: './section-trataments.component.html',
  styleUrls: ['./section-trataments.component.css']
})
export class SectionTratamentsComponent implements OnInit {

  loading = true;
  textLoading = "Cargando tratamientos... ";
  trataments : ITratament[];
  allTrataments : ITratament[];
  constructor(private _tratamentsService: TratamentService) { }

  async ngOnInit() {
    this.allTrataments = await this._tratamentsService.getTrataments();
    this.trataments = this.allTrataments.slice(0,3);
    this.loading = false;
  }

}
