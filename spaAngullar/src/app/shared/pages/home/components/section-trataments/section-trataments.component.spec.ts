import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionTratamentsComponent } from './section-trataments.component';

describe('SectionTratamentsComponent', () => {
  let component: SectionTratamentsComponent;
  let fixture: ComponentFixture<SectionTratamentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionTratamentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionTratamentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
