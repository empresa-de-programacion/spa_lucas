import { Component, Input, OnInit } from '@angular/core';
import { ITratament } from '../../interfaces/itratament';

@Component({
  selector: 'app-tratament',
  templateUrl: './tratament.component.html',
  styleUrls: ['./tratament.component.css']
})
export class TratamentComponent implements OnInit {

  @Input() tratament: ITratament;
  constructor() { }

  ngOnInit(): void {
    if(!this.tratament.url_image || this.tratament.url_image === undefined){
      this.tratament.url_image = "../../../../assets/images/no_image.png";
    }
  }


}
