import { Component, OnInit } from '@angular/core';
import { ESessionKeys } from '../../enums/ESessionKeys';
import { LoginService } from '../../services/login.service';
import { LocaleSessionUtilService } from '../../utils/locale-session-util.service';

@Component({
  selector: 'app-nav-admin',
  templateUrl: './nav-admin.component.html',
  styleUrls: ['./nav-admin.component.css']
})
export class NavAdminComponent implements OnInit {

  user;
  constructor(
    private _loginService : LoginService,
    private _LocaleSessionUtilService: LocaleSessionUtilService
  ) { }

  ngOnInit(): void {
    this.user = this._LocaleSessionUtilService.getJsonInSession(ESessionKeys.USER_IN_SESSION);
    }

  handleClickCloseSession(){
    this._loginService.logOut();
  }
}
