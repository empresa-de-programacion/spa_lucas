import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ABMCosmeticsComponent } from './shared/pages/admin/abmcosmetics/abmcosmetics.component';
import { ABMNutritionComponent } from './shared/pages/admin/abmnutrition/abmnutrition.component';
import { ABMTratamentsComponent } from './shared/pages/admin/abmtrataments/abmtrataments.component';
import { LoginComponent } from './shared/pages/admin/login/login.component';
import { CosmeticsComponent } from './shared/pages/cosmetics/cosmetics.component';
import { HomeComponent } from './shared/pages/home/home.component';
import { NutritionComponent } from './shared/pages/nutrition/nutrition.component';
import { ServiciosComponent } from './shared/pages/servicios/servicios.component';
import { AuthGuardService } from './shared/services/auth-guard.service';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'tratamientos', component: ServiciosComponent },
  { path: 'cosmeticos', component: CosmeticsComponent },
  { path: 'nutricion', component: NutritionComponent },
  { path: 'admin', component: LoginComponent },
  { path: 'admin/login', component: LoginComponent },
  { path: 'admin/tratamientos', component: ABMTratamentsComponent, canActivate: [AuthGuardService]},
  { path: 'admin/cosmeticos', component: ABMCosmeticsComponent, canActivate: [AuthGuardService] },
  { path: 'admin/nutricion', component: ABMNutritionComponent, canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
