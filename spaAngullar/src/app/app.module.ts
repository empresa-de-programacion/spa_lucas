import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './shared/pages/home/home.component';
import { LayoutComponent } from './shared/components/layout/layout.component';
import { NavComponent } from './shared/components/nav/nav.component';
import { SectionBannerTextComponent } from './shared/pages/home/components/section-banner-text/section-banner-text.component';
import { SectionTratamentsComponent } from './shared/pages/home/components/section-trataments/section-trataments.component';
import { TratamentComponent } from './shared/components/tratament/tratament.component';
import { WspContactComponent } from './shared/components/wsp-contact/wsp-contact.component';
import { FooterComponent } from './shared/components/footer/footer.component';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { NgxLoadingModule } from 'ngx-loading';
import { LoadingComponent } from './shared/components/loading/loading.component';
import { SectionMapComponent } from './shared/pages/home/components/section-map/section-map.component';
import { SectionMediaSocialComponent } from './shared/pages/home/components/section-media-social/section-media-social.component';
import { ServiciosComponent } from './shared/pages/servicios/servicios.component';
import { FormsModule } from '@angular/forms';
import { SectionFreeDiagnosticComponent } from './shared/pages/home/components/section-free-diagnostic/section-free-diagnostic.component';
import { ABMTratamentsComponent } from './shared/pages/admin/abmtrataments/abmtrataments.component';
import { LayoutAdminComponent } from './shared/components/layout-admin/layout-admin.component';
import { NavAdminComponent } from './shared/components/nav-admin/nav-admin.component';
import { ABMCosmeticsComponent } from './shared/pages/admin/abmcosmetics/abmcosmetics.component';
import { ABMNutritionComponent } from './shared/pages/admin/abmnutrition/abmnutrition.component';
import { LoginComponent } from './shared/pages/admin/login/login.component';
import * as firebase from 'firebase';
import { CosmeticsComponent } from './shared/pages/cosmetics/cosmetics.component';
import { NutritionComponent } from './shared/pages/nutrition/nutrition.component';
import { SectionCosmeticsComponent } from './shared/pages/home/components/section-cosmetics/section-cosmetics.component';
import { SectionNutritionComponent } from './shared/pages/home/components/section-nutrition/section-nutrition.component';

firebase.default.initializeApp(environment.firebase);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LayoutComponent,
    NavComponent,
    SectionBannerTextComponent,
    SectionTratamentsComponent,
    TratamentComponent,
    WspContactComponent,
    FooterComponent,
    LoadingComponent,
    SectionMapComponent,
    SectionMediaSocialComponent,
    ServiciosComponent,
    SectionFreeDiagnosticComponent,
    ABMTratamentsComponent,
    LayoutAdminComponent,
    NavAdminComponent,
    ABMCosmeticsComponent,
    ABMNutritionComponent,
    LoginComponent,
    CosmeticsComponent,
    NutritionComponent,
    SectionCosmeticsComponent,
    SectionNutritionComponent,
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase, 'spalucas'),
    BrowserModule,
    AppRoutingModule,
    AngularFirestoreModule,
    NgxLoadingModule.forRoot({}),
    FormsModule


    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
