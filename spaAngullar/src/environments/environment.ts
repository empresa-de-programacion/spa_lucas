// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCc1uXkG4P8_43C32l45IxGYWgt5V2i8ko",
    authDomain: "spalucas.firebaseapp.com",
    projectId: "spalucas",
    storageBucket: "spalucas.appspot.com",
    messagingSenderId: "74306050322",
    appId: "1:74306050322:web:0357418ec6ffe31e537bcb"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
